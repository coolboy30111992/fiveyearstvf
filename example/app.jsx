import React from "react";
import ReactDOM from "react-dom";
import Avatar from "../src/avatar.jsx";
import downloadjs from 'downloadjs';
import html2canvas from 'html2canvas';
class App extends React.Component {
  constructor(props) {
    super(props);
    const src = SOURCE_PATH + "/frame.png";
    this.state = {
      preview: null,
      defaultPreview: null,
      src,
    };
    this.onCrop = this.onCrop.bind(this);
    this.onCropDefault = this.onCropDefault.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onCloseDefault = this.onCloseDefault.bind(this);
    this.onLoadNewImage = this.onLoadNewImage.bind(this);
  }

  onCropDefault(preview) {
    this.setState({ defaultPreview: preview });
  }

  onCrop(preview) {
    this.setState({ preview });
  }

  onCloseDefault() {
    this.setState({ defaultPreview: null });
  }

  onClose() {
    this.setState({ preview: null });
  }

  onLoadNewImage() {
    const src = SOURCE_PATH + "/einshtein2.jpeg";
    this.setState({ src });
  }

  async handleCaptureClick(){
    const pricingTableElmt =
      document.querySelector('.hehe');
    if (!pricingTableElmt) return;

    const canvas = await html2canvas(pricingTableElmt);
    const dataURL = canvas.toDataURL('image/png');
    downloadjs(dataURL, 'download.png', 'image/png');
  };

  render() {
    return (
      <div className="container-fluid">
        <div
          className="row"
          style={{ backgroundColor: "#a75d61", padding: "8px 0" }}
        >
          <div className="col-2" />
          <div className="col-8">
            
            <h1
              style={{
                marginTop: "3px",
                color: "white",
                fontWeight: 300,
                fontSize: "2rem",
              }}
            >
            TECHVIFY 5th Anniversary
            </h1>
          </div>
          <div className="col-2" />
        </div>
        <div className="row" style={{ marginTop: "45px" }}>
          <div className="col-2" />
         
          <div className="col-2" />
        </div>
        <div className="row">
          <div className="col-2" />
          <div className="col-5">
            <Avatar
              width={390}
              height={295}
              exportSize={390}
              onCrop={this.onCropDefault}
              onClose={this.onCloseDefault}
              exportAsSquare
            />
          </div>
          <div className="col-2">
      
            <div className="hehe" style={{ width: "500px", height: "500px", position:'relative', textAlign:'center' }}>
              <img
              alt=""
              style={{ width: "500px", height: "500px" }}
              src={this.state.defaultPreview}
            />
            <img src={this.state.src} alt="" style={{ width: "500px", height: "500px", position:'absolute', left:'0', bottom:'0' }}/>
             <button style={{marginTop:"20px"}} onClick={()=>{this.handleCaptureClick()}}>Download</button>
            </div>
          </div>
          <div className="col-3" />
         
        </div>
       
        
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
